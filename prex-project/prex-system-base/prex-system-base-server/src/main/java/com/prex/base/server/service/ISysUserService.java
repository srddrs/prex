package com.prex.base.server.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.prex.base.api.dto.RepeatCheckDTO;
import com.prex.base.api.dto.UserDTO;
import com.prex.base.api.dto.UserDetailsInfo;
import com.prex.base.api.entity.SysUser;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.Set;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author lihaodong
 * @since 2019-04-21
 */
public interface ISysUserService extends IService<SysUser> {

    /**
     * 分页查询用户信息（含有角色信息）
     *
     * @param page    分页对象
     * @param userDTO 参数列表
     * @return
     */
    IPage<SysUser> getUsersWithRolePage(Page page, UserDTO userDTO);

    /**
     * 保存用户以及角色部门等信息
     * @param userDto
     * @return
     */
    boolean insertUser(UserDTO userDto);

    /**
     * 更新用户以及角色部门等信息
     * @param userDto
     * @return
     */
    boolean updateUser(UserDTO userDto);

    /**
     * 删除用户信息
     * @param userId
     * @return
     */
    boolean removeUser(Integer userId);

    /**
     * 重置密码
     * @param userId
     * @return
     */
    boolean restPass(Integer userId,String password);

    /**
     * 获取授权用户信息
     * @param sysUser
     * @return
     */
    UserDetailsInfo findUserInfo(SysUser sysUser);

    /**
     * 通过用户名查找用户个人信息
     *
     * @param username 用户名
     * @return 用户信息
     */
    SysUser findUserInByName(String username);

    /**
     * 根据用户id查询权限
     * @param userId
     * @return
     */
    Set<String> findPermsByUserId(Integer userId);

    /**
     * 通过用户id查询角色集合
     * @param userId
     * @return
     */
    Set<String> findRoleIdByUserId(Integer userId);


    boolean register(UserDTO userDTO);

    /**
     * 修改用户信息
     * @param sysUser
     * @return
     */
    boolean updateUserInfo(SysUser sysUser);

    /**
     * 根据用户id / 用户名 / 手机号查询用户
     * @param userIdOrUserNameOrPhone
     * @return
     */
    SysUser findUserByUserIdOrUserNameOrPhone(String userIdOrUserNameOrPhone);


    /**
     * 数据校验
     * @param repeatCheckDTO
     * @return
     */
    boolean repeatCheck(RepeatCheckDTO repeatCheckDTO);

    /**
     * 根据社交类型和社交id查询社交用户信息
     * @param providerId
     * @param providerUserId
     * @return
     */
    SysUser getUserBySocial(String providerId,int providerUserId);


}
